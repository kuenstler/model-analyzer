https://www.npmjs.com/package/fast-xml-parser

webpack it:

$ npm install webpack webpack-cli --save-dev

put this into your main .js (which is defined in package.json):

import xml_parser from 'fast-xml-parser';
window.XMLParser = xml_parser;

$ npx webpack --entry 'C:\Users\alexander.hausmann\Documents\model-analyzer'

your packed js is in /dist/null.js
